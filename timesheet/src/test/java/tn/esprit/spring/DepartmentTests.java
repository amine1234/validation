package tn.esprit.spring;


import java.text.ParseException;


import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import tn.esprit.spring.controller.ControllerEmployeImpl;
import tn.esprit.spring.controller.ControllerTimesheetImpl;
import tn.esprit.spring.controller.RestControlEntreprise;
import tn.esprit.spring.entities.Departement;
import tn.esprit.spring.entities.Employe;
import tn.esprit.spring.entities.Mission;
import tn.esprit.spring.entities.MissionExterne;
import tn.esprit.spring.entities.Role;


@RunWith(SpringRunner.class)
@SpringBootTest
public class DepartmentTests {

	private static final Logger l = LogManager.getLogger(DepartmentTests.class);
	
	@Autowired
	ControllerEmployeImpl employeControl;
	
	@Autowired
	ControllerTimesheetImpl timesheetControl;	
	
	@Autowired
	RestControlEntreprise RestCEControl;
	
	@Test
	public void test() throws ParseException {

		try {

		
	
			Employe KhaledKallel = new Employe ("kallel","khaled","Khaled.kallel@siiconsulting.tn",true,Role.INGENIEUR);
			Employe mohamedZitouni = new Employe ("zitouni","mohamed","mohamed.zitouni@siiconsulting.tn",true,Role.TECHNICIEN);
			Employe aymenOuali = new Employe ("ouali","aymen","aymen.ouali@siiconsulting.tn",true,Role.INGENIEUR);
			Employe bochraBouzid = new Employe ("bouzid","bochra","Khaled.kallel@siiconsulting.tn",true,Role.CHEF_DEPARTEMENT);
			Employe yosraArbi = new Employe ("arbi","yosra","Khaled.kallel@siiconsulting.tn",true,Role.CHEF_DEPARTEMENT);
			
			int KhaledKallelId = employeControl.ajouterEmploye(KhaledKallel);
			int mohamedZitouniId = employeControl.ajouterEmploye(mohamedZitouni);
			int aymenOualiId = employeControl.ajouterEmploye(aymenOuali);
			int bochraBouzidId = employeControl.ajouterEmploye(bochraBouzid);
			int yosraArbiId = employeControl.ajouterEmploye(yosraArbi);
			
			employeControl.ajouterEmploye(KhaledKallel);
			employeControl.ajouterEmploye(mohamedZitouni);
			employeControl.ajouterEmploye(aymenOuali);
			employeControl.ajouterEmploye(bochraBouzid);
			employeControl.ajouterEmploye(mohamedZitouni);


			
			Departement Telecom = new Departement(1,"Telecom");
			Departement RH = new Departement(2,"RH");
			
			int TelecomId = RestCEControl.ajouterDepartement(Telecom);
			int RHId = RestCEControl.ajouterDepartement(RH);
			
			RestCEControl.ajouterDepartement(Telecom);
			RestCEControl.ajouterDepartement(RH);
			
			employeControl.affecterEmployeADepartement(KhaledKallelId, RHId);
			employeControl.affecterEmployeADepartement(KhaledKallelId, TelecomId);

			employeControl.affecterEmployeADepartement(mohamedZitouniId, TelecomId);
			employeControl.affecterEmployeADepartement(mohamedZitouniId, RHId);
			
			employeControl.affecterEmployeADepartement(aymenOualiId, TelecomId);
			employeControl.affecterEmployeADepartement(bochraBouzidId, RHId);
			employeControl.affecterEmployeADepartement(yosraArbiId, TelecomId);

			MissionExterne missionExterne1 = new MissionExterne ("MissionExterne","Mise en place du 4G pour l'entreprise ..","facturation@orange.tn", 650);
			MissionExterne missionExterne2 = new MissionExterne ("MissionExterne","Dev d'un nouveau outil de vente","facturation@orange.tn", 475);
			Mission mission = new Mission ("Mission","Maintenance de SIRH interne");
			
			int missionExterne1Id = timesheetControl.ajouterMission(missionExterne1);
			int missionExterne2Id = timesheetControl.ajouterMission(missionExterne2);
			int missionId = timesheetControl.ajouterMission(mission);
			
			timesheetControl.ajouterMission(missionExterne1);
			timesheetControl.ajouterMission(missionExterne2);
			timesheetControl.ajouterMission(mission);
			
			timesheetControl.affecterMissionADepartement(missionExterne1Id, TelecomId);
			timesheetControl.affecterMissionADepartement(missionExterne2Id, TelecomId);
			timesheetControl.affecterMissionADepartement(missionId, RHId);
			
			
		l.info(employeControl.getEmployePrenomById(KhaledKallelId));
		l.info("test");
		l.info(RestCEControl.ajouterDepartement(Telecom));
		l.info(RestCEControl.ajouterDepartement(RH));
		l.info(timesheetControl.ajouterMission(missionExterne1));
		l.info(timesheetControl.ajouterMission(missionExterne2));
		l.info(timesheetControl.ajouterMission(mission));
		}
		
		
		catch (Exception e) { l.error("Erreur dans getAllDepartments() : " + e); }
	}
		
		
		}